const GIPHY_URL =
  'https://api.giphy.com/v1/gifs/translate?api_key=tpaHeLvAj1Vn6nWt9BoClrax0UvLuW81&s=';

domReady(function() {
  document.querySelector('form').addEventListener('change', getAdjective);
});

function getAdjective(event) {
  const menu = document.querySelector('select');
  event.preventDefault();
  const adjective = menu.value;
  getData(adjective);
}

function getData(searchTerm) {
  const container = document.querySelector('.container');
  fetch(GIPHY_URL + encodeURIComponent(searchTerm))
    .then(function(response) {
      return response.json();
    })
    .then(function(giphyData) {
      const gif = document.querySelector('.gif');
      gif.setAttribute('src', giphyData.data.embed_url);
      container.appendChild(gif);
    })
    .catch(function(err) {
      console.log(err.message);
    });
}

function domReady(f, o, m) {
  (o = document), (m = 'addEventListener');
  o[m] ? o[m]('DOMContentLoaded', f) : ((o = window), o.attachEvent('onload', f));
}
